
- Example notebook at

  openpose-data-analysis-sample.ipynb

- Data (key points & video/images) are stored in

  data/db/

   keypoint-sample.sqlite3
   keypoint-sample.tar.xz

- SQLite3 db and TAR archive are accessed with Peewee and libarchive respectively.

  VideoImageDB is used to read both video and images in the TAR archive.

- Q.constraint is used to load a specific set of data with the "constraint" given by, for example,

  lambda Q : ( Q._set == 'elderfall', Q._category == 'outdoor_fall', Q._video ==   5777, Q._camera == 1 )

- Once Q is supplied with the SQLite3 database, bind_table is used to provide access to various tables in it.

  . bind_table provides aliases, via introspection, to simplify coding (refer to Q.constraint).
  . a custom column is defined for MaskRCNN data to access packed (compressed joblib dump) as JSON

VIM required to "see" the code as intended with fold merkers === & ///.

