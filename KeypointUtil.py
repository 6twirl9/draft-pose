# === Import...
import os

#
# Direct the libarchive module to the location of the libarchive library
# (ref. libarchive/library.py)
#
# Adopt it to your system which may require a different path.
#
os.environ['LA_LIBRARY_FILEPATH'] = \
 '/usr/local/Cellar/libarchive/3.3.2/lib/libarchive.dylib'
#'/project/at073-elderfall/opt/libarchive/lib/libarchive.so'

import sys
import re
import io
import time
import inspect
import operator
import joblib
import magic
from numbers import Number
import base64

#
# Iterable video frames
#
import pims

#
# Image processing
#
import cv2 as cv
ocv = cv

#
# Archive file I/O
#
import libarchive.public as rxv

import webcolors

from datetime import date
import numpy as np
#from cgat import RLE

import peewee
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase, JSONField

from sklearn.cluster import KMeans
import seaborn as sbrn
import matplotlib.pyplot as plot

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import IPython # ... display.HTML

#///

def _flatten(*args):             # ===

 args_ = [] ;   # ===

 for _ in args:

  if isinstance(_,(list,tuple)):

   args_.extend(_)

  else:

   args_.append(_)

#///

 return args_

#///

class VideoImageDB:   # ===

 class Image:   # ===

  def __init__(self,data):

   self.data = []

   for _ in sorted(data,key=operator.itemgetter(0)):

    self.data.append([*_[:3],ocv.cvtColor(ocv.imdecode(np.asarray(bytearray(_[3]),dtype=np.uint8),1),ocv.COLOR_BGR2RGB)])

   self.n = len(self.data)
   self.shape = self[0].shape

  def play(self,h,w):

   pass

  def __len__(self): return self.n

  def __iter__(self):

   self._iterIndex = 0
   return self

  def __next__(self):

   if self._iterIndex < self.n:

    ret = self.data[self._iterIndex]

    self._iterIndex += 1

    return ret

   else:

    raise StopIteration()

  def __getitem__(self,i):

   if i >= 0:
    i = i % self.n
   else:
    i = - ( (-i) % self.n )

   return self.data[i][3]

#///
 class Video:   # ===

  def __init__(self,data):

   self.data = data[:3]
   self.stream = data[3]

   self.b = io.BytesIO(data[3])
   self.data.append( pims.PyAVReaderTimed(self.b) )

   self.n = len(self.data[3])

   self.shape = self[0].shape

  def play(self,h,w=None):

   if w is None:

    s = h
    h = int(self.shape[0]*s)
    w = int(self.shape[1]*s)

   video = base64.b64encode(self.stream)

   return IPython.display.HTML(data='''<video alt="test" controls height="{0}" width="{1}"">
                <source src="data:video/mp4;base64,{2}" type="video/mp4" />
             </video>'''.format(h,w,video.decode('ascii')))

  def __len__(self): return self.n

  def __iter__(self):

   self._iterIndex = 0
   return self

  def __next__(self):

   if self._iterIndex < self.n:

    ret = self.data[3][self._iterIndex]

    self._iterIndex += 1

    return ret

   else:

    raise StopIteration()

  def __getitem__(self,i):

   if i >= 0:
    i = i % self.n
   else:
    i = - ( (-i) % self.n )

   return self.data[3][i]

#///

 class example: # ===

  def __init__(self):  # ===

   self.constaints = \
   [
     lambda Q : ( Q._set == 'elderfall'      , Q._category == 'outdoor_fall'        , Q._video == 5777  , Q._camera == 1 )
   , lambda Q : ( Q._set == 'elderfall'      , Q._category == 'indoor_fall'         , Q._video == 5824  , Q._camera == 1 )
   , lambda Q : ( Q._set == 'elderfall-extra', Q._category == '190110Fall.fix.crf22', Q._video == 113110, Q._camera == 1 )
   , lambda Q : ( Q._set == 'URFall'         , Q._category == 'fall'                , Q._video == 4     , Q._camera == 0 )
   , lambda Q : ( Q._set == 'Lancet-5'       , Q._category == ''                    , Q._video == 2     , Q._camera == 1 )
   , lambda Q : ( Q._set == 'MultiCam'       , Q._category == 'chute'               , Q._video == 19    , Q._camera == 6 )
   , lambda Q : ( Q._set == 'Joannie'        , Q._category == ''                    , Q._video == 3     , Q._camera == 1 )
   , lambda Q : ( Q._set == 'FDD'            , Q._category == 'Office'              , Q._video == 3     , Q._camera == 1, Q._run == 2 )
   ]

#///

  def __call__(self,index=None):   # ===

   if index is None: return

   o = VideoImageDB(index=index)

   for which in ['original','openpose']:

    print(f'-- {which} -- '+'-'*64)

    for constraint in constraints:

     o(constraint,which)

#///

#///

 def __init__(self,original=None,openpose=None,index=None): # ===

  db = \
  {
    'original': '/project/at073-elderfall/collection/archive/DATA'
  , 'openpose': '/project/at073-elderfall/collection/data/OpenPose/keypoint-2019-01-31.tar'
  }

  for _ in ['original','openpose']:

   it = locals()[_]

   if it is not None: db[_] = it

  self.index = index

  self.db = db

#///

 def __call__(self,constraint,which='original',listing_only=False,no_fetch_data=False):            # ===

  Z, V = self.index

  self.shape = what1(select(Z,'height','width').where(*constraint(Z)))

  ret = list(V.select().join(Z,on=(Z._file==V._file)).where(*constraint(Z)).tuples())

  if len(ret) != 1: return None

  dset, archive, file, _type, openpose = ret[0]

  if which == 'original':   # ===

   archive_ = '/'.join([self.db[which],dset,archive])

   ret = tarxiv_fetch(archive_,file)

   _, expand, size, data = ret[0]  

   #
   # image -> expand = True | no staging on disk so should be of reasonable size
   #
   # pass this to another handler which will enforce the order
   #

   if _type == 'image':

    ret = []

    for each in tarxiv_fetch(data,listing_only=listing_only,no_fetch_data=no_fetch_data):

     ret.append(each)

    ret = VideoImageDB.Image(ret)

   else:

    ret = VideoImageDB.Video(ret[0])

#///
  if which == 'openpose':   # ===

   ret = tarxiv_fetch(self.db['openpose'],openpose)

   _, expand, size, data = ret[0]  

   if expand:

    ret = []

    for each in tarxiv_fetch(data,'/image.mp4',listing_only=listing_only,no_fetch_data=no_fetch_data):

     ret.append(each)

    ret = VideoImageDB.Video(ret[0])

#//

  return ret

#///

#///

#///

#
# Archive stuff
#
def isArchive(_):    # ===

 ans = False
 with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:

  id = None

  if isinstance(_,str):

   id = m.id_filename(_)

  if isinstance(_,bytes):

   id = m.id_buffer(_)

  if id is not None:

   ans = id in [f'application/{_}' for _ in ['zip','x-tar']]

 return ans

#///
def tarxiv_fetch(archive,file=None,listing_only=False,no_fetch_data=False):  # ===

 collect = []

 def fetch(_,pat=None):  # ===

  collect = []

  if pat is not None:

   pat = pat.replace('(','\\(').replace(')','\\)')

  with rxv.memory_reader(_) as a:

   for e in a:

    if not e.filetype.IFREG: continue

    matched = re.match(f'^.*/?{pat}',e.pathname) if pat is not None else True

    if matched or (pat is None and listing_only):

     if not listing_only:

      data = b''.join([ b for b in e.get_blocks() ])

      ret = [ e.pathname, isArchive(data[:1024]), len(data) ] 

      if not no_fetch_data: ret.append(data)

     else:

      ret = e.pathname

     collect.append(ret)

  return collect

#///

 if isinstance(archive,str):

  with open(archive,'rb') as o:

    collect = fetch(o.read(),file)

 if isinstance(archive,bytes):

  collect = fetch(archive,file)

 return collect

#///
def tarxiv_list(file,check_magic=False):  # ===

 collect = []

 with open(file,'rb') as o:

  with rxv.memory_reader(o.read()) as a:

   for e in a:

    if re.match('keypoint/FIX',e.pathname): continue

    if e.filetype.IFREG:

     ret = e.pathname

     if callable(check_magic):

      ret = [ret,check_magic(next(e.get_blocks())[:1024])]

     collect.append(ret)

 return collect

#///

#
# SQL db stuff
#
select1    = lambda Q, *rest : Q.select(*[getattr(Q,f'_{_}') for _ in rest]).limit(1).tuples()
select     = lambda Q, *rest : Q.select(*[getattr(Q,f'_{_}') for _ in rest]).tuples()
what1      = lambda _        : list(_)[0] if _.count() > 0 else []
what       = lambda _        : list(_)    if _.count() > 0 else []

class MRCNN_BBOX_MASK:  # ===

#base64charset = ''.join([*[getattr(string,_) for _ in ['ascii_uppercase','ascii_lowercase','digits']],'-','_'])

 @staticmethod
 def int2base64(_):          # ===

  if isinstance(_,str): _ = int(_)

  i = 0
  c = []
  a, b = np.divmod(_,64)

  c.append(base64charset[b])

  while a > 0: 

   a, b = np.divmod(a,64)
   c.append(base64charset[b])

  ret = ''.join(c)

  return ret

 #///

 @staticmethod
 def rle_encode(mask):       # ===
     """Encodes a mask in Run Length Encoding (RLE).
     Returns a string of space-separated values.
     """
     assert mask.ndim == 2, "Mask must be of shape [Height, Width]"
     # Flatten it column wise
     shape = mask.shape
     m = mask.T.flatten()
     # Compute gradient. Equals 1 or -1 at transition points
     g = np.diff(np.concatenate([[0], m, [0]]), n=1)
     # 1-based indicies of transition points (where gradient != 0)
     rle = np.where(g != 0)[0].reshape([-1, 2]) + 1
     # Convert second index in each pair to lenth
     rle[:, 1] = rle[:, 1] - rle[:, 0]
     return " ".join(map(str, rle.flatten())), shape

 #///

 @staticmethod
 def rle_decode(rle, shape): # ===
     """Decodes an RLE encoded list of space separated
     numbers and returns a binary mask."""
     rle = list(map(int, rle.split()))
     rle = np.array(rle, dtype=np.int32).reshape([-1, 2])
     rle[:, 1] += rle[:, 0]
     rle -= 1
     mask = np.zeros([shape[0] * shape[1]], np.bool)
     for s, e in rle:
         assert 0 <= s < mask.shape[0]
         assert 1 <= e <= mask.shape[0], "shape: {}  s {}  e {}".format(shape, s, e)
         mask[s:e] = 1
     # Reshape and transpose
     mask = mask.reshape([shape[1], shape[0]]).T
     return mask

 #///

 def __init__(self,file=None):  # ===

  if file is not None:

   self.load_joblib(file)

#///

 def load_joblib(self,file):    # ===

  self.data = joblib.load(file)

  return self

#///

 def toJSON(self,pack=True):    # ===

  data_ = []
  for _ in self.data:

   roi  = _[0]['rois']
   mask = _[0]['masks'].transpose([2,0,1]) if len(_[0]['masks']) > 0 else np.array([])

   size = mask.shape[1:]

   collect = []
   shape   = []

   for r, m in zip(roi,mask):

#   r = r[ [0,2,1,3] ]
    r = [ r[_] for _ in [0,2,1,3] ]

    if pack:
     m = m[r[0]:r[1]+1,r[2]:r[3]+1].astype(int)
    else:
     m = m.astype(int)

    encoded, _shape = MRCNN_BBOX_MASK.rle_encode(m)

    collect.append(np.array(list(map(int,encoded.split()))).astype(np.int32).tolist())

    shape.append(_shape)

   _[0]['masks'] = collect
#  for tag in ['rois','class_ids','scores']:
#   _[0][tag] = _[0][tag].tolist()

   _[0]['pack']  = pack
   _[0]['size']  = size
   _[0]['shape'] = shape

   data_.append(_[0])

  self.data = data_

  return self

 #///
 def deJSON(self):              # ===

  data_ = []
  for _ in self.data:

   for tag in ['rois','class_ids','scores']:
    _[tag] = np.array(_[tag])

   roi  = _['rois']
   mask = _['masks']

   pack  = _['pack']
   size  = _['size']
   shape = _['shape']

   collect = []

   for r, m, s in zip(roi,mask,shape):

    m = MRCNN_BBOX_MASK.rle_decode(' '.join(map(str,m)),s)

    r = r[ [0,2,1,3] ]

    if pack:
     m_ = np.zeros(size)
     m_[r[0]:r[1]+1,r[2]:r[3]+1] = m
    else:
     m_ = m

    m_ = m_.astype(bool)

    collect.append(m_)

#  print('DEBUG: ',np.array(collect).shape,len(np.array(collect).shape),len(collect))

   _['masks'] = np.array(collect).transpose([1,2,0]) if len(collect) > 0 else np.array([])
   for tag in ['rois','class_ids','scores']:
    _[tag] = _[tag].tolist()

   del _['size']
   del _['pack']
   del _['shape']

   data_.append([_])

  self.data = data_

  return self

 #///

 def pack(self,data=None):      # ===

  if data is None:

   data = self.data

  with io.BytesIO() as b:
   joblib.dump(data,b,compress=('xz',9))
   self.data = b.getvalue()

  return self

#///

 def unpack(self,data=None):    # ===

  if data is not None:

   with io.BytesIO(data) as b:

    self.data = joblib.load(b)

  return self

#///

 def __call__(self,data=None):            # ===

  if data is not None:

   self.data = data

   return self

  else:

   return self.data

#///
 def __getitem__(self,i):       # ===

  return self.data[i]

#///

#///
class BboxMRCNNBlobField(Field):  # ===

 field_type = 'blob'

 def db_value(self,value):

  o = MRCNN_BBOX_MASK()

  return o(value).toJSON().pack()()
  
 def python_value(self,value):

  o = MRCNN_BBOX_MASK()

  return o.unpack(value).deJSON()()

#///

def _BaseModel (db):            # ===

 class BaseModel(Model):   # ===

  class PrimaryKey: pass

  @classmethod
  def define_column(cls,dialect='SQLite3'):  # ===

   cls_locals = inspect.currentframe().f_back.f_locals

   col_t_map = \
   {
     'SQLite3':
     {
       'TEXT'   : 'Text'
     , 'INTEGER': 'Integer'
     , 'REAL'   : 'Double'
     , 'JSON'   : 'JSON'
     , 'BLOB'   : 'Blob'
     , 'BboxMRCNNBlob' : 'BboxMRCNNBlob'
     }
   }[dialect]

   for c, t, *rest in cls_locals['column_definition']:

    decl_t = globals()[f'{col_t_map[t]}Field']

    _ = decl_t(column_name=c)

    if rest:

     if rest[0] == BaseModel.PrimaryKey:

      _.primary_key = True

    cls_locals [f'_{c}'] = _

#///

  @classmethod
  def _create_table(cls):

   db.create_tables([cls])

   return cls

  class Meta:

   legacy_table_names=False

   database    = db

#///

 return BaseModel

#///
def _KeypointBody25 (db,table=None):  # ===

 BaseModel = _BaseModel(db)

 class KeypointBody25(BaseModel):

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT'    )
   , ( 'category', 'TEXT'    )
   , ( 'run'     , 'INTEGER' )
   , ( 'video'   , 'INTEGER' )
   , ( 'camera'  , 'INTEGER' )
   , ( 'frame'   , 'INTEGER' )

   , ( 'people'  , 'INTEGER' )
   , ( 'keypoint', 'JSON'    )
   , ( 'file'    , 'TEXT'    )
  #, ( 'file'    , 'TEXT'    , BaseModel.PrimaryKey )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = "keypoint/body25"

   indexes = \
   (
     (('set','category','run','video','camera','frame'),True)
   ,
   )

 return KeypointBody25

#///
def _DotMetaNumberOfFrame(db):             # ===

 BaseModel = _BaseModel(db)

 class DotMetaNumberOfFrame(BaseModel):  # ===

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT'    )
   , ( 'category', 'TEXT'    )
   , ( 'run'     , 'INTEGER' )
   , ( 'video'   , 'INTEGER' )
   , ( 'camera'  , 'INTEGER' )

   , ( 'frame'   , 'INTEGER' )
   , ( 'file'    , 'TEXT'    )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = ".meta/#frame"

   indexes = \
   (
     (('set','category','run','video','camera'),True)
   ,
   )

#///

 return DotMetaNumberOfFrame

#///
def _DotMetaImageSize(db):             # ===

 BaseModel = _BaseModel(db)

 class DotMetaImageSize(BaseModel):  # ===

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT'    )
   , ( 'category', 'TEXT'    )
   , ( 'run'     , 'INTEGER' )
   , ( 'video'   , 'INTEGER' )
   , ( 'camera'  , 'INTEGER' )

   , ( 'height'  , 'INTEGER' )
   , ( 'width'   , 'INTEGER' )
   , ( 'file'    , 'TEXT'    )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = ".meta/image/size"

   indexes = \
   (
     (('set','category','run','video','camera'),True)
   ,
   )

#///

 return DotMetaImageSize

#///
def _DotMetaAnnotationFrameFall(db):   # ===

 BaseModel = _BaseModel(db)

 class DotMetaAnnotationFrameFall(BaseModel):  # ===

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT'    )
   , ( 'category', 'TEXT'    )
   , ( 'run'     , 'INTEGER' )
   , ( 'video'   , 'INTEGER' )
   , ( 'camera'  , 'INTEGER' )

   , ( 'fall'    , 'INTEGER' )
   , ( 'frame'   , 'JSON'    )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = ".meta/annotation/frame/fall"

   indexes = \
   (
     (('set','category','run','video','camera'),True)
   ,
   )

#///

 return DotMetaAnnotationFrameFall

#///
def _BboxSSD(db):                  # ===

 BaseModel = _BaseModel(db)

 class BboxSSD(BaseModel):  # ===

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT'    )
   , ( 'category', 'TEXT'    )
   , ( 'run'     , 'INTEGER' )
   , ( 'video'   , 'INTEGER' )
   , ( 'camera'  , 'INTEGER' )

   , ( 'bbox'    , 'JSON'    )
   , ( 'file'    , 'TEXT'    )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = "bbox/ssd"

   indexes = \
   (
     (('set','category','run','video','camera'),True)
   ,
   )

#///

 return BboxSSD

#///
def _BboxMRCNN(db):                  # ===

 BaseModel = _BaseModel(db)

 class BboxMRCNN(BaseModel):  # ===

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT'    )
   , ( 'category', 'TEXT'    )
   , ( 'run'     , 'INTEGER' )
   , ( 'video'   , 'INTEGER' )
   , ( 'camera'  , 'INTEGER' )

#  , ( 'bbox'    , 'BLOB'    )
   , ( 'bbox'    , 'BboxMRCNNBlob'    )
   , ( 'file'    , 'TEXT'    )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = "bbox/mrcnn"

   indexes = \
   (
     (('set','category','run','video','camera'),True)
   ,
   )

#///

 return BboxMRCNN

#///
def _DotMetaArchiveVideoImage(db):             # ===

 BaseModel = _BaseModel(db)

 class DotMetaArchiveVideoImage(BaseModel):  # ===

  # === Column definitions (name,type,primary key?)

  column_definition = \
   (
     ( 'set'     , 'TEXT' )
   , ( 'archive' , 'TEXT' )
   , ( 'file'    , 'TEXT' )

   , ( 'type'    , 'TEXT' )
   , ( 'openpose', 'TEXT' )
   )

#///

  BaseModel.define_column()

  class Meta:

   primary_key = False
   table_name  = ".meta/archive/video+image"

   indexes = \
   (
     (('set','archive','file'),True)
   ,
   )

#///

 return DotMetaArchiveVideoImage

#///

def bind_table(db,*args,bind=None,**kargs):   # === _create_tables

 t = { **kargs, **{ _: _ for _ in _flatten(args) if _ not in kargs } }

 encl_locals = inspect.currentframe().f_back.f_locals

#print('DEBUG:',encl_locals.keys())

 collect = {}

 for b, a in t.items() :

  collect[a] = globals()[f'_{b}'](db)._create_table()

 # Uses encl_locals whether inside IPython or not

 if '__doc__' in encl_locals and encl_locals['__doc__'] is not None and 'IPython' in encl_locals['__doc__']:

  for k, v in collect.items(): encl_locals[k] = v

 else:

  for k, v in collect.items():

  #globals()[k] = v
   encl_locals[k] = v

 if bind is not None and ( inspect.isclass(type(bind)) or isinstance(bind,dict) ):

# print('Bind to instance:',bind)

  for k, v in collect.items():

   setattr(bind,k,v)

 return list(collect.keys())

#///
def _columns(table,*args):      # ===

 return [getattr(table,f'_{_}') for _ in _flatten(args)]

#///
def _fuzzy_rle(d,relation='rle',compress=True): # ===

 if len(d) == 0: return []

 if not callable(relation):

  if relation is None or relation == 'rle':
   relation = lambda a, b: b == a
  if relation == 'range':
   relation = lambda a, b: (b-a) == 1

 c = [[d[0],d[0],1]]
 for i in d[1:]:
  if not relation(c[-1][1],i):
   c.append([i,i,1])
  else:
   c[-1][1]  = i
   c[-1][2] += 1

 compressable = all([ _[0] == _[1] for _ in c])

 if compressable:

  c = [ _[1:] for _ in c ]

 return c

#///

#
# Key points stuff
#

#
# Plot time series
#
_cache = {}

def clear_all_data():   # ===
 
 _cache = {}

#///
def clear_data(_set,_category,_video,_camera):  # ===
 
 tag = (_set,_category,_video,_camera)
 
 if tag in _cache:
  
  print('DELETE: ',tag)
  
  del _cache[tag]

#///
def load_data(K,_set='',_category='',_video=-1,_camera=-1,_run=1,cut=0,constraint=None):    # ===
 
 magic = []
 
 if constraint is not None:
  
  it = { re.sub('^_','',_.lhs.name): _.rhs for _ in ( constraint(K) if callable(constraint) else constraint ) }
  
  _set      = it['set']
  _category = it['category']
  _video    = it['video']
  _camera   = it['camera']
  _run      = it.get('run',1)
 
 tag = (_set,_category,_video,_camera)
 
 if tag in _cache:
  
  print('RELOAD: ',tag)
  
  return _cache[tag]
 
 R = K \
 .select(K._file,K._frame,K._keypoint) \
 .where(K._set==_set,K._category==_category,K._video==_video,K._camera==_camera).order_by(K._frame)
#.where(K._set=='MultiCam',K._category=='chute',K._video==5,K._camera==3)

 print(f'# data points: {R.count()}')

 x = { _: [] for _ in _body_25_index['definition'].values()}

 for _ in R:

  #print(_._file)
  for i, people in enumerate(_._keypoint['people']):

   d =  np.array(people['pose_keypoints_2d']).reshape(-1,3)
   
   c = np.sum(d[:,2]**2)
   magic.append(c)
   
 #  if cut > 0 and c < cut: continue
 #  if cut < 0 and c > abs(cut): continue

   collect = {}

   for k, v in _body_25_index['region'].items():

    if k not in collect: collect[k] = {}

    for i in v:

     collect[k][_body_25_index['definition'][i]] = d[i].tolist()
   
   for region, parts in collect.items():
    for part, XYC in parts.items():
     x[part].append([_._frame,*XYC])

 x = { k: np.array(v) for k, v in x.items() }

 del x['Background']
 
 _cache[tag] = x
 
 print('CACHED: ',tag)
 
 magic = np.array(magic)
 
 return x #, magic

#///
def plot_data(x,s=np.s_[:],conf=True,anchor=None,mode=1,aggregate=np.median,window=None,axis={},xlim=None,ylim=None,save=None,figsize=(30,20),figscale=1):   # ===
 
 axis = {'show':False,'share':True,**axis}
 if anchor is not None:
  u = None
  for which in ['region','level']:
   if anchor in _body_25_index[which]:
    ref = np.array([ x[_] for _ in [ _body_25_index['definition'][_] for _ in _body_25_index[which][anchor] ]]).transpose([1,2,0])
    u = np.array([ [ aggregate(b) for b in a ] for a in ref ])
    #print(ref.shape,u.shape)
    #print(u[:1])
    break
 
 plot.rcParams['figure.figsize'] = np.array(figsize)*figscale
 for region, group in enumerate(_body_25_index['region'].values()):
  
  i = region*6
  
  q = [ [] for _ in range(3) ]
  for i, k in enumerate([ _body_25_index['definition'][_] for _ in group ]):
   v = x[k]
   z = u if anchor is not None and u is not None else v
   if mode == 0:
    z_ = [ z[s,_] for _ in [1,2]]
   else:
    if window is None:
     z_ = [ [ max(z[s,_][0:a+1]) for a in range(len(z[s,_])) ] for _ in [1,2] ]
    else:
     z_ = [ [ max(z[s,_][max(0,a+1-window):a+1]) for a in range(len(z[s,_])) ] for _ in [1,2] ]
   #print(v[s][:1])
   for i in [1,2]:
#   d = v[s,i]/z_[i-1]
    with np.errstate(divide='ignore', invalid='ignore'):
     d = np.true_divide(v[s,i],z_[i-1])
    if len(d[np.isfinite(d)]) > 0:
     q[i-1].append(np.max(d[np.isfinite(d)]))
    else:
     q[i-1].append(0)
  #  q[i-1].append(v[s,i]/max(z_[i-1]))
   q[2].append(np.max(v[s,3]))
  
  #print(q)
  q = max([ np.max(_) for _ in np.array(q).reshape(3,-1) ]) * 1.05
  #print(q)
   
  for i, k in enumerate([ _body_25_index['definition'][_] for _ in group ]):
   v = x[k]
   z = u if anchor is not None and u is not None else v
   if window is None:
    z_ = [ [ max(z[s,_][0:a+1]) for a in range(len(z[s,_])) ] for _ in [1,2] ]
   else:
    z_ = [ [ max(z[s,_][max(0,a+1-window):a+1]) for a in range(len(z[s,_])) ] for _ in [1,2] ]

   ax = plot.subplot(5,6,region*6+i+1)
   if i > 0 and not axis['show']:
    ax.yaxis.set_visible(False)
   plot.title(k)
   


   color = ['blue','orange']
   for i in [1,2]:
    if mode == 1:
     with np.errstate(divide='ignore', invalid='ignore'):
      d=v[s,i]/z_[i-1]
     plot.plot(v[s,0],d,'o',markersize=2,label=i)   
    #plot.plot(v[s,0],v[s,i]/z_[i-1],'o',markersize=2,label=i)   
    else:
     plot.plot(v[s,0],v[s,i]/max(z_[i-1]),'o',markersize=2,label=i)

   if conf:
    plot.plot(v[s,0],v[s,3],'o',markersize=3,label=i,alpha=0.25)
    
   if xlim is not None:
    ax.set_xlim(*xlim)
   if axis['share']:
    if ylim is None:
     ax.set_ylim(0,q)
    else:
     ax.set_ylim(max(0,ylim[0]),min(q,ylim[1]))

 plot.subplots_adjust(wspace=0.025,hspace=0.275)

 if save is not None:

  plot.savefig(save)

#///

#
# Helpers
#
# === body 25 model parts

_body_25_index = \
{
  'definition':
  {
    0: 'Nose'       #  0
  , 1: 'Neck'       #  1
  , 2: 'RShoulder'  #  2
  , 3: 'RElbow'     #  3
  , 4: 'RWrist'     #  4
  , 5: 'LShoulder'  #  5
  , 6: 'LElbow'     #  6
  , 7: 'LWrist'     #  7

  , 8: 'MidHip'

  , 9: 'RHip'       #  8
  , 10: 'RKnee'     #  9
  , 11: 'RAnkle'    # 10
  , 12: 'LHip'      # 11
  , 13: 'LKnee'     # 12
  , 14: 'LAnkle'    # 13

  , 15: 'REye'      # 14
  , 16: 'LEye'      # 15
  , 17: 'REar'      # 16
  , 18: 'LEar'      # 17

  , 19: 'LBigToe'
  , 20: 'LSmallToe'
  , 21: 'LHeel'
  , 22: 'RBigToe'
  , 23: 'RSmallToe'
  , 24: 'RHeel'

  , 25: 'Background'
  }
, 'level':
  {
    'nose': [0]
  , 'neck': [1]
  , 'eye': [15,16,17,18]
  , 'shoulder': [2,5]
  , 'elbow': [3,6]
  , 'wrist': [4,7]
  , 'hip': [9,8,12]
  , 'knee': [10,13]
  , 'ankle': [11,14]
  , 'foot': [22,23,24,19,20,21] # [19,20,21,22,23,24]
  }
, 'region':
  {
    'top'   : [ 0, 1, 15, 16, 17, 18 ]
  , 'upper' : [ 2, 5, 3, 6 ]
  , 'middle': [ 4, 7, 9, 8, 12 ]
  , 'lower' : [ 10, 13, 11, 14 ]
  , 'bottom': [ 22,23,24,19,20,21 ] # [ 21, 24, 19, 20, 22, 23 ]
 #, 'other' : [ 25 ]
  }
}

#///

def sort_pose_keypoints_2d(l):  # ===
 
 l_ = [_[0] for _ in sorted([ (i,*np.mean(_[...,:2],axis=0)) for i,_ in enumerate(l) ],key=operator.itemgetter(1,2))]

 return [l[_] for _ in l_]

#///
def pose_keypoints_2d(coord_,prune=[],confidence=True,skullcap=True,skullcap_stretch=1,sort=True):  # ===
 
 if isinstance(coord_,dict) and 'people' in coord_:
  
  ret = \
  [
    pose_keypoints_2d \
    (
      _
    , prune=prune
    , confidence=confidence
    , skullcap=skullcap
    , skullcap_stretch=skullcap_stretch
    )
    for _ in coord_['people']
  ]
  
  if sort:
   
   ret = sort_pose_keypoints_2d(ret)
   
  return ret

 # modify a copy
 coord = coord_.copy()
 
 missing = lambda _ : _[0] == 0 and _[1] == 0

 if not isinstance(coord,np.ndarray):
  
  coord = np.array(coord['pose_keypoints_2d']).reshape(-1,3)
 
 if not confidence and coord.shape[-1] == 3:
  
  coord = coord[...,:2]
 
 index = _body_25_index
 parts = { v: k for k, v in index['definition'].items() }
 
 todel = []
 
 for this in prune:
  
  if this in parts:           todel.append(parts[this])
  if this in index['level']:  todel.extend(index['level'][this])
  if this in index['region']: todel.extend(index['region'][this])
   
 nose, neck = [ coord[parts[_]] for _ in ['Nose','Neck'] ]
 
 if skullcap:
  
  if not ( missing(nose) or missing(neck) ):
  
   coord = np.vstack((coord,(nose + skullcap_stretch*(nose - neck))))
  
   if confidence:
   
    coord[-1][-1] = min(nose[-1],neck[-1])
    
  else:
   
   coord = np.vstack((coord,[0,0,0] if confidence else [0,]))
   
 for that in todel:
  
  coord[that][:] = 0
  
 return coord

#///
def pose_keypoints_2d_parts_identified(kp,verbose=False,group_by=None,sym={},confidence=False,spacing=(2,0),truncate=4): # ===

 if spacing[1] is None: spacing[1] = 0

 sym = { 'present': '\u2588', 'absent': '\u2581', **sym }

 def post(s):

  if confidence:

   # Light shade
   absent = '\u2591' if sym['absent'] == '\u2581' else sym['absent']

   return str(s[0]).replace('0',absent).replace('1',chr(0x2581+int(8*s[1])))

  else:

   return str(s[0]).replace('0',sym['absent']).replace('1',sym['present'])

 if isinstance(kp,(tuple,list)) and len(kp) == 2 and isinstance(kp[0],Number):

  kp_ = kp[1]

 else:

  kp_ = kp
 
 ret = [ [ [ 0 if part[0] == 0 and part[1] == 0 else 1, part[2], part[:2] ] for part in person ] for person in kp_ ]

 if verbose:

  if group_by is None or group_by == 'None':

#  [ print(' '.join([str(x[0]).replace('0',sym['absent']).replace('1',sym['present']) for x in _])) for _ in ret ]
#  [ print(' '.join([post(x) for x in _])) for _ in ret ]
   for i, _ in enumerate(ret):

    n = len(np.array([ z[2] for z in _ if z[0] == 1 ]))
    x, y = np.int0(np.array([ z[2] for z in _ if z[0] == 1 ]).mean(axis=0))

    print(f'{i:2d} ({x:4d},{y:4d}) [{n:2d}] ' + (' '*spacing[0]).join([post(x) for x in _]) )

  else:

   if group_by == 'definition':

    _body_25_index_definition = { v[:truncate] : [int(k)] for k, v in _body_25_index[group_by].items() }

    header = list(_body_25_index_definition.keys())
    pat = (' '*spacing[0]).join([ f'{{:>{max(len(s),-spacing[1]+(1+spacing[1])*len(_body_25_index_definition[s]))}s}}' for s in _body_25_index_definition.keys() ])

   else:

    header = list(_body_25_index[group_by].keys())
    pat = (' '*spacing[0]).join([ f'{{:>{max(len(s),-spacing[1]+(1+spacing[1])*len(_body_25_index[group_by][s]))}s}}' for s in _body_25_index[group_by].keys() ])

   header = pat.format(*header)
   print(('{:20s}'.format(''))+header)
   print(('{:20s}'.format(''))+'-'*len(header))

   for i, _ in enumerate(ret):

    n = len(np.array([ z[2] for z in _ if z[0] == 1 ]))
    x, y = np.int0(np.array([ z[2] for z in _ if z[0] == 1 ]).mean(axis=0))

    h = []

    for k, v in ( _body_25_index[group_by] if group_by != 'definition' else _body_25_index_definition ).items():

#    h.append(''.join([ str(_[i][0]) for i in v ]).replace('0',sym['absent']).replace('1',sym['present']))
     h.append((' '*spacing[1]).join([ post(_[j]) for j in v ]))

    #print(' '.join(h))
    print(f'{i:2d} ({x:4d},{y:4d}) [{n:2d}] ' + pat.format(*h))

 ret = [ [ 0 if part[0] == 0 and part[1] == 0 else 1 for part in person ] for person in kp_ ]

 return ret

#///
def pose_keypoints_2d_parts_merge     (kp,verbose=False): # ===

 if isinstance(kp,(tuple,list)) and len(kp) == 2 and isinstance(kp[0],Number):

  kp_ = kp[1]

 else:

  kp_ = kp

 kp_  = np.array(kp_).transpose([2,1,0])
 pick = np.array([ sorted(list(enumerate(_)),key=operator.itemgetter(1))[-1] for _ in kp_[2,...] ])
 ret  = np.array([ *[ [ b[a] for a, b in zip(np.int0(pick[:,0]),kp_[i,...]) ] for i in range(2) ], pick[:,1].tolist() ]).transpose()
 
 return ret

#///

#
# Bounding boxes
#
class ROI:  # ===

 # === Test data
 test_bbox = \
 [
   # === 0
   {
     0:
     [
       [388.1419372558594, 84.86022186279297, 517.8861083984375, 352.2381286621094, 0.9980022311210632]
     , [167.12030029296875, 50.16423797607422, 314.81561279296875, 292.2297058105469, 0.9304830431938171]
     ]
   , 1:
     [
       [369.0182800292969, 72.29352569580078, 521.1048583984375, 357.6285400390625, 0.9998005032539368]
     , [170.10760498046875, 38.40852355957031, 322.50091552734375, 291.9056701660156, 0.944969117641449]
     ]
   , 2:
     [
       [371.41473388671875, 72.09254455566406, 519.752685546875, 356.2615661621094, 0.9996671676635742]
     , [163.68263244628906, 28.560718536376953, 322.1668701171875, 290.4757995605469, 0.9010181427001953]
     ]
   , 3:
     [
       [384.085205078125, 84.3106689453125, 513.6015014648438, 339.22308349609375, 0.9995964169502258]
     , [177.16763305664062, 52.389530181884766, 322.62603759765625, 288.8439025878906, 0.9706028699874878]
     ]
   , 4:
     [
       [367.45111083984375, 73.89411926269531, 524.4550170898438, 356.5067443847656, 0.999842643737793]
     , [175.21243286132812, 36.39177322387695, 327.59375, 294.2319641113281, 0.956476628780365]
     ]
   }
#///
   # === 1
 , {
     0:
     [
       [176.8031463623047, 30.651329040527344, 281.11676025390625, 230.97987365722656, 0.9961453676223755]
     , [287.4888916015625, 49.272586822509766, 382.4331359863281, 236.1597442626953, 0.9862581491470337]
     , [202.81675720214844, 44.17659378051758, 347.97479248046875, 256.0467224121094, 0.5418401956558228]
     ]
   , 1:
     [
       [169.60693359375, 13.72342586517334, 279.2115783691406, 235.4950714111328, 0.9965247511863708]
     , [271.1149597167969, 35.98326110839844, 396.46661376953125, 256.72113037109375, 0.9786776304244995]
     ]
   , 2:
     [
       [168.40536499023438, 13.108030319213867, 279.3559875488281, 227.84996032714844, 0.9966049194335938]
     , [267.5735168457031, 33.578895568847656, 397.2124938964844, 251.93251037597656, 0.960780143737793]
     ]
   , 3:
     [
       [175.39590454101562, 29.97684097290039, 272.4042663574219, 229.43212890625, 0.9980370402336121]
     , [281.232421875, 49.94847106933594, 384.43603515625, 233.3026123046875, 0.9974581599235535]
     ]
   , 4:
     [
       [170.270263671875, 15.75013542175293, 279.4385986328125, 232.5336151123047, 0.9951534271240234]
     , [272.2900085449219, 36.24504470825195, 396.2933044433594, 255.60546875, 0.9801105260848999]
     ]
   }
#///
   # === 2
 , {
     0:
     [
       [176.8031463623047, 30.651329040527344, 281.11676025390625, 230.97987365722656, 0.9961453676223755]
     , [287.4888916015625, 49.272586822509766, 382.4331359863281, 236.1597442626953, 0.9862581491470337]
     , [287.4888916015625, 49.272586822509766, 382.4331359863281, 236.1597442626953, 0.29862581491470337]
     ]
   , 1:
     [
       [169.60693359375, 13.72342586517334, 279.2115783691406, 235.4950714111328, 0.9965247511863708]
     , [271.1149597167969, 35.98326110839844, 396.46661376953125, 256.72113037109375, 0.9786776304244995]
     ]
   , 2:
     [
       [168.40536499023438, 13.108030319213867, 279.3559875488281, 227.84996032714844, 0.9966049194335938]
     , [267.5735168457031, 33.578895568847656, 397.2124938964844, 251.93251037597656, 0.960780143737793]
     ]
   , 3:
     [
       [175.39590454101562, 29.97684097290039, 272.4042663574219, 229.43212890625, 0.9980370402336121]
     , [281.232421875, 49.94847106933594, 384.43603515625, 233.3026123046875, 0.9974581599235535]
     ]
   , 4:
     [
       [170.270263671875, 15.75013542175293, 279.4385986328125, 232.5336151123047, 0.9951534271240234]
     , [272.2900085449219, 36.24504470825195, 396.2933044433594, 255.60546875, 0.9801105260848999]
     ]
   }
#///
   # === 3
 , {
     0:
     [
       [176.8031463623047, 30.651329040527344, 281.11676025390625, 230.97987365722656, 0.9961453676223755]
     , [287.4888916015625, 49.272586822509766, 382.4331359863281, 236.1597442626953, 0.9862581491470337]
     , [202.81675720214844, 44.17659378051758, 347.97479248046875, 256.0467224121094, 1.0]
     ]
   , 1:
     [
       [169.60693359375, 13.72342586517334, 279.2115783691406, 235.4950714111328, 0.9965247511863708]
     , [271.1149597167969, 35.98326110839844, 396.46661376953125, 256.72113037109375, 0.9786776304244995]
     ]
   , 2:
     [
       [168.40536499023438, 13.108030319213867, 279.3559875488281, 227.84996032714844, 0.9966049194335938]
     , [267.5735168457031, 33.578895568847656, 397.2124938964844, 251.93251037597656, 0.960780143737793]
     ]
   , 3:
     [
       [175.39590454101562, 29.97684097290039, 272.4042663574219, 229.43212890625, 0.9980370402336121]
     , [281.232421875, 49.94847106933594, 384.43603515625, 233.3026123046875, 0.9974581599235535]
     ]
   , 4:
     [
       [170.270263671875, 15.75013542175293, 279.4385986328125, 232.5336151123047, 0.9951534271240234]
     , [272.2900085449219, 36.24504470825195, 396.2933044433594, 255.60546875, 0.9801105260848999]
     ]
   }
#///
   # === 4
 , {
     0:
     [
       [176.8031463623047, 30.651329040527344, 281.11676025390625, 230.97987365722656, 0.9961453676223755]
     , [287.4888916015625, 49.272586822509766, 382.4331359863281, 236.1597442626953, 0.9862581491470337]
     , [202.81675720214844, 44.17659378051758, 347.97479248046875, 256.0467224121094, 1.0]
     ]
   }
#///
   # === 5
 , {
     0:
     [
       [176.8031463623047, 30.651329040527344, 281.11676025390625, 230.97987365722656, 0.9961453676223755]
     , [287.4888916015625, 49.272586822509766, 382.4331359863281, 236.1597442626953, 0.9862581491470337]
     , [202.81675720214844, 44.17659378051758, 347.97479248046875, 256.0467224121094, 1.0]
     , [169.60693359375, 13.72342586517334, 279.2115783691406, 235.4950714111328, 0.9965247511863708]
     , [271.1149597167969, 35.98326110839844, 396.46661376953125, 256.72113037109375, 0.9786776304244995]
     ]
   }
#///
 ]

#///

 @staticmethod
 def area(r):                   # ===

  return (r[2]-r[0])*(r[3]-r[1])

#///

 @staticmethod
 def IoU(r0,r1,epsilon=1e-7):   # ===
 
  i = ROI.intersection(r0,r1)

  A = lambda _ : ROI.area(_) * ( 1 if len(_) == 4 else _[-1] )

  return A(i)/max(A(r0)+A(r1)-A(i),epsilon)

#///

 @staticmethod
 def intersection(r0,r1):       # ===
 
  pair = lambda r, i : [ r[0][i[0]],r[1][i[0]],r[0][i[1]],r[1][i[1]]]

  x = np.array(pair( (r0,r1) if r0[0] < r1[0] else (r1,r0),(0,2) ))
  y = np.array(pair( (r0,r1) if r0[1] < r1[1] else (r1,r0),(1,3) ))

  overlap = True
  if x[2] <= x[1] or y[2] <= y[1]:
   overlap = False
   i =  [0,0,0,0]
  else:
   x = sorted(x)
   y = sorted(y)
   i = [x[1],y[1],x[2],y[2]]

  if len(r0) == 5 and len(r1) == 5:

   i.append( 0 if not overlap else r0[-1] * r1[-1] )

  return i

#///

 @staticmethod
 def union(r0,r1):              # ===
 
  pair = lambda r, i : [ r[0][i[0]],r[1][i[0]],r[0][i[1]],r[1][i[1]]]

  x = np.array(pair( (r0,r1) if r0[0] < r1[0] else (r1,r0),(0,2) ))
  y = np.array(pair( (r0,r1) if r0[1] < r1[1] else (r1,r0),(1,3) ))

  overlap = True
  if x[2] <= x[1] or y[2] <= y[1]:
   overlap = False
   u =  [0,0,0,0]
  else:
   x = sorted(x)
   y = sorted(y)
   u = [x[0],y[0],x[3],y[3]]

  if len(r0) == 5 and len(r1) == 5:

   u.append( 0 if not overlap else r0[-1] * r1[-1] )

  return u

#///

 @staticmethod
 def IaU(r0,r1):                # ===
 
  pair = lambda r, i : [ r[0][i[0]],r[1][i[0]],r[0][i[1]],r[1][i[1]]]

  x = np.array(pair( (r0,r1) if r0[0] < r1[0] else (r1,r0),(0,2) ))
  y = np.array(pair( (r0,r1) if r0[1] < r1[1] else (r1,r0),(1,3) ))

  overlap = True
  if x[2] <= x[1] or y[2] <= y[1]:
   overlap = False
   i =  [0,0,0,0]
  else:
   x = sorted(x)
   y = sorted(y)
   i = [x[1],y[1],x[2],y[2]]
   u = [x[0],y[0],x[3],y[3]]

  if len(r0) == 5 and len(r1) == 5:

   i.append( 0 if not overlap else r0[-1] * r1[-1] )
   u.append( 0 if not overlap else r0[-1] * r1[-1] )

  return i, u

#///

 @staticmethod
 def intersections(rs):         # ===

  i = rs[0]

  for r in rs[1:]:

   i = ROI.intersection(i,r)

  return i

#///

 @staticmethod
 def unions(rs):                # ===

  u = rs[0]

  for r in rs[1:]:

   u = ROI.union(u,r)

  return u

#///

 @staticmethod
 def IaUs(rs):                  # ===

  i, u = rs[0], rs[0]

  if len(rs) > 1:
   for r in rs[1:]:

    i = ROI.intersection(i,r)
    u = ROI.union       (u,r)

  return i, u

#///

 @staticmethod
 def group(bbox,n_cluster=0,detail=False,merge_singleton=True):   # ===

  # === bbox_, n_cluster_guess

  bbox_ = []

  if isinstance(bbox,dict):

   cluster = {}

   for model, boxes in sorted(bbox.items(),key=lambda _:_[0]):

    n = len(boxes)

    if n not in cluster: cluster[n] = 0

    cluster[n] += 1

    for i, box in enumerate(boxes):

     bbox_.append( [ (model,i), box ] )

   #
   # - Zero sized bbox which no other bounding boxes should overlap!
   # - Identify the class which contains all the unconnected pairs.
   #
   bbox_.append( [ (None,-1), [0]*5 ] )

   #
   # Mode
   #
   n_cluster_guess = sorted( cluster.items(), key = lambda _ : _[1] )[-1][0]

  else:

   bbox_ = list(enumerate(bbox))

   bbox_.append([ -1, [0]*5 ])

   n_cluster_guess = n_cluster

#///

  #
  # Connectivity
  #
  pairs = []

  for i in range(len(bbox_)):
   for j in range(i,len(bbox_)):

    pairs.append([i,j,ROI.IoU(bbox_[i][-1],bbox_[j][-1])])

  pairs = np.array(pairs)

 #print('#bbox',len(bbox_))
 #print('#cluster guess',n_cluster_guess)
 #print('#pairs',len(pairs))

  pairs_ = {}
# for n in range(max(1,n_cluster_guess-1),n_cluster_guess+1):
  for n in range(2,max(2,n_cluster)+1):

   if len(pairs) < n: continue

#  print('HERE',len(pairs),n)

   #
   # Construct estimator
   #
   e = KMeans(init='k-means++',n_clusters=n,n_init=10)
   e.fit(pairs[:,-1:])

   #
   # Predict & classify
   #
   r = [ [] for _ in range(n) ]

   for _ in pairs:

    score = int(e.predict([ _[-1:] ]))
    r[score].append(_)

   #
   # Connectivity matrix
   #
   m = len(bbox_)
   x = np.zeros((m,m))
   for i, _ in enumerate(r):
    for a, b, *rest in _:

     a, b = int(a), int(b)

     x[a,b] = i
     x[b,a] = i

   null = int(x[-1,0])
   transl = { k: (v+1) for v, k in enumerate(sorted(set(range(n))-{null})) }
   transl[null] = 0

   x = np.array([ transl[_] for _ in  x.copy().ravel()]).reshape(x.shape)[:-1,:-1].astype(np.int64)

 # print(x)

   #
   # Groups
   #
   grp = []

   for i in range(m-1):
    which = []
    for k, _ in enumerate(grp):
     if i in _:
      which.append(k)
    if len(which) == 0:
     grp.append({i})
     which = [len(grp)-1]
    for j in range(m-1):
     if x[i,j] > 0:
      for _ in which:
       grp[_].add(j)

#  print(r)
#  print(x)
#  print(grp)

   #
   # Need to sort according to the top left corner !
   #

   grp = [ set(_) for _ in set([tuple(sorted(tuple(_))) for _ in grp])]
  #grp = [ _ for _ in sorted(grp,key=lambda _:len(_)) if len(_) != 1 ]
   grp = [ _ for _ in sorted(grp,key=lambda _: min(_)) if len(_) != 1 ]

#  print(grp)
#  print(len(grp),n)
   

   if True: #len(grp) >= n:

    collect = { _: [] for _ in ['intersection','union'] }

    grp_ = set()
    for _ in grp:
     grp_ = grp_.union(_)
    not_in_grp = list(set(range(len(bbox_)-1))-grp_)

    if merge_singleton:

     grp.extend([set([_]) for _ in not_in_grp])

    for _ in grp:

     i, u = ROI.IaUs([ bbox_[a][-1] for a in _ ])

     collect['intersection'].append(i)
     collect['union']       .append(u)

    order = [ _[0] for _ in sorted([ (id,*_[:2]) for id, _ in enumerate(collect['intersection']) ],key=operator.itemgetter(1,2)) ]

    grp   = [ grp[_] for _ in order ]

    for k, v in collect.items():

     collect[k] = [ collect[k][_] for _ in order ]

    pairs_[n] = { 'group': grp, 'cluster': n, 'bbox': bbox_[:-1], 'bbox_i': collect['intersection'], 'bbox_u': collect['union'] }

    if detail:
     pairs_[n] = { **pairs_[n], 'estimator': e, 'prediction': r, 'connectivity': x }

  return pairs_

#///

 @staticmethod
 def heatmap(t,ncol=1):         # ===

  m = len(t.keys())

  for n, _ in t.items():

   x = _['connectivity']
   n = _['cluster']

   mask = np.zeros_like(x)
   mask[np.triu_indices_from(mask)] = True
   f, ax = plot.subplots((m//ncol)+(1 if (m%ncol) > 0 else 0),ncol)
   with sbrn.axes_style("white"):
    #for ax_ in ax:
    sbrn.heatmap(x, mask=mask, vmax=n-1, square=True,  cmap="YlGnBu",ax=ax)
   plot.show()

#///

 @staticmethod
 def example(which=4):                 # ===

  print({ cluster: _['group'] for cluster, _ in ROI.group(ROI.test_bbox[which]).items() })

#///

#///
def inside_bbox(b,p):   # ===
 
 if isinstance(p[0],(list,tuple,np.ndarray)):
  
  return [ inside_bbox(b,_) for _ in p ]
 
 return 1 if b[0] <= p[0] <= b[2] and b[1] <= p[1] <= b[3] else 0

#///
class bboxSSD:  # ===

 def __init__(self,bbox,tag=15,group=True):

  self.mode('intersection',True)

  self._bbox = bbox

  self.pruned = None
  self.prune(tag=tag,group=group)

 def prune(self,tag=15,group=True):  # ===
 
  # Only interested in person, i.e. class 15
 
  prune = lambda box : [ np.array(_)[[2,3,4,5,1]].tolist() for _ in box if _[0] == tag ]
 
  # split by time
 
  m = list(self._bbox.keys())
  n = len(self._bbox[m[0]])
  t = [ { model: [] for model in m } for _ in range(n) ]
 
  for model, frames in self._bbox.items():
  
   for i, frame in enumerate(frames):
   
    t[i][model] = sorted(prune(frame),key=operator.itemgetter(0,1))
   
  if group:

   t = [ ROI.group(_) for _ in t ]

  self.pruned = t
  self.prune_opt = { 'tag': tag, 'group': group }

  self.n = len(self.pruned)
   
  return self

#///

 def mode(self,which=None,round=None):  # ===

  if which is not None: self._which = which
  if round is not None: self._round = round

  return self

#///

 def __len__(self): return self.n

 def __getitem__(self,i):   # ===

  if i >= 0:
   i = i % self.n
  else:
   i = - ( (-i) % self.n )

  which = { 'intersection': 'bbox_i', 'union': 'bbox_u', 'i': 'bbox_i', 'u': 'bbox_u' }[self._which]

  if 2 in self.pruned[i]:

   ret = self.pruned[i][2][which]

   if self._round:

   #fuz = np.array([-0.5,-0.5,0.5,0.5])
    fuz = np.array([-0.0,-0.0,1.0,1.0])
    ret = np.int0([ _ + fuz for _ in np.array(ret)[:,:4] ]).tolist()

  else:

   ret = []
  
  return ret

#///

 def __iter__(self):    # ===

  self._iterIndex = 0

  return self

#///

 def __next__(self):    # ===

  if self._iterIndex < self.n:

   ret = self[self._iterIndex]

   self._iterIndex +=  1

   return ret

  else:

   raise StopIteration()

#///

 def bbox(self,i):  # ===

  return self.pruned[i][2]

#///

#///
class bboxMRCNN:  # ===

 def __init__(self,bbox,tag=1): # ===

  self.mode(True)

  self._bbox = bbox

  self.pruned = None
  self.prune(tag=tag)

#///

 def mode(self,swap_xy=None):   # ===

  if swap_xy is not None: self._swap_xy = swap_xy

  return self

#///

 def prune(self,tag=1):     # ===

  self.pruned = []

  for i, _ in enumerate(self._bbox):

   collect = []
   class_ids = _[0]['class_ids']
   rois      = _[0]['rois']
   for id, rois  in zip(class_ids,rois):
    if id == tag:
#    rois = np.array(rois)[[1,0,3,2]].tolist()
     collect.append(rois)

   self.pruned.append(collect)

  self.prune_opt = { 'tag': tag }

  self.n = len(self.pruned)
   
  return self

#///

 def bbox(self,i):          # ===

  return self.pruned[i]

#///

 def __len__(self):         # ===

  return self.n

#///
 def __iter__(self):        # ===

  self._iterIndex = 0

  return self

#///
 def __next__(self):        # ===

  if self._iterIndex < self.n:

   ret = self[self._iterIndex]

   self._iterIndex +=  1

   return ret

  else:

   raise StopIteration()

#///
 def __getitem__(self,i):   # ===

  if i >= 0:
   i = i % self.n
  else:
   i = - ( (-i) % self.n )

  rois = self.pruned[i]

  if self._swap_xy:

   if len(rois) > 0:
    rois = np.array(rois)[:,[1,0,3,2]].tolist()

  return rois

#///

#///

#
# Regions where:
#
# 0) neither SSD nor Mask RCNN manages to identify a person
# 1) either SSD or Mask RCNN identifies a person
#
# (1) is indicated by '<---', number of dots proportional to the length of time it is "active"
#
def check_bbox_support(shape,*bbox,bins=20,density=True,verbose=False,figsize=(6,4)):   # ===

 ret = {}

 h, w = shape[:2]

 area    = [[] for _ in range(2)]
 area_   = [[] for _ in range(2)]
 collect = [[] for _ in range(2)]
 detail  = [[] for _ in range(6)]

 for i, (a, b) in enumerate(zip(*bbox)):

  for j, ab in enumerate([a,b]):

   area [j].append([(x1-x0)*(y1-y0)/(h*w) for x0, y0, x1, y1 in ab])
   area_[j].extend(area[j][-1])

  collect[len(a) + len(b) > 0      ].append(i)
  detail [2*(len(a)>0) + (len(b)>0)].append(i)

  if len(a) > 0: detail[4].append(i)
  if len(b) > 0: detail[5].append(i)

 if verbose:
  if not density:
   label = [ '{:4d} {:12s}'.format(len(detail[4+_]),bbox[_].__class__.__name__) for _ in range(2)]
  else:
   label = [ bbox[_].__class__.__name__ for _ in range(2)]
  plot.figure(figsize=figsize)
  plot.hist(sorted(area_[0]),alpha=0.3,bins=bins,label=label[0],density=density)
  plot.hist(sorted(area_[1]),alpha=0.3,bins=bins,label=label[1],density=density)
  plot.title('Bbox area distributions (normalized to total area)')
  if density:
   plot.ylabel('area = 1')
  else:
   plot.ylabel('# frames')
  plot.legend()

 ret['duration'] = len(bbox[0])
 ret['identified'] = \
 {
   'not': len(detail[0])
 , 'solo':
   {
     re.sub('^bbox','',bbox[0].__class__.__name__): len(detail[2])
   , re.sub('^bbox','',bbox[1].__class__.__name__): len(detail[1])
   }
 , 'total':
   {
     re.sub('^bbox','',bbox[0].__class__.__name__): len(detail[4])
   , re.sub('^bbox','',bbox[1].__class__.__name__): len(detail[5])
   }
 , 'intersection': len(detail[3])
 , 'union': len(detail[1])
 }

 if verbose:
  print('Total duration:',len(bbox[0]))
  print('Identified:')
  print(' ... {:10s}: {:4d}'.format('neither',len(detail[0])))
  print(' ... {:10s}: {:4d} (solo) \/ {:4d} (total)'.format(bbox[0].__class__.__name__,len(detail[2]),len(detail[4])))
  print(' ... {:10s}: {:4d} (solo) /\ {:4d} (total)'.format(bbox[1].__class__.__name__,len(detail[1]),len(detail[5])))
  print(' ... {:10s}: {:4d} (intersection)'.format('both',len(detail[3])))
  print(' ... {:10s}: {:4d} (union)'.format('either',len(collect[1])))
  print('-'*64)
  plot.show()
  print('')
  print('')

 merged = []
 for i, _ in enumerate(collect):
  merged.extend([ [*_,i] for _ in _fuzzy_rle(_,'range')])
 span = [ [ _[2]  for _ in merged if _[3] == i ] for i in range(2) ]

 ret['span'] = merged

 if verbose:
  print('{:>5s} {:>5s} {:>5s}'.format('START','END','SPAN'))
  for _ in sorted(merged,key=lambda _ : _[0]):
   print('{:5d} {:5d} {:5d} {:4s} {}'.format(*_[:3],'___/' if _[3] == 1 else '',('.'*int(_[2]/max(span[1])*100)) if _[3] == 1 else ''))

 return ret

#///

#
# class Q gives you access to all the data
#
class Q:    # ===

 def copy(self,db_file=None,_constraint=None,verbose=False):    # ===

  if _constraint is None: _constraint = self._constraint
  if not isinstance(_constraint,(list,tuple)): _constraint = [ _constraint ]

  def transfer(de,a,q,constraint=None):    # ===

   if callable(constraint) and len(q) > 0:

    it = { re.sub('^_','',_.lhs.name): _.rhs for _ in constraint(getattr(de,q[0])) }
    print('CONSTRAINT:',it)

   else:

    print('CONSTRAINT:',None)

   for _ in q:

    elapsed = []
    elapsed.append(time.time())

    if verbose:

     print(f' ... {_:2s} ',end='')

    fetch = getattr(de,_) .select()

    if callable(constraint):

     fetch = fetch .where(*constraint(getattr(de,_)))

    fetch = list(fetch.tuples())

    if verbose:

     print(f'{len(fetch):4d} ',end='')

    ret = 0

    if len(fetch) > 0:

     with self.db.atomic():

      for chunk in chunked(fetch,999):

       ret += \
        getattr(a,_) \
         .insert_many \
         (
           chunk
         , fields=getattr(a,_)._meta.sorted_fields
         ) \
         .on_conflict_ignore() \
         .execute()

    elapsed.append(time.time())

    if verbose:

     print(f'{ret:4d} {elapsed[-1]-elapsed[-2]:6.3f}')

#///

  other = Q(db_file)

  elapsed = []

  elapsed.append(time.time())

  transfer(self,other,self._tables_alias['constraint'][False],None)

  elapsed.append(time.time())
  print(f'/ / {len(_constraint)} ' + '-' * 128, ' {:6.3f}s'.format(elapsed[-1]-elapsed[-2]))

  for i, each in enumerate(_constraint):

   elapsed.append(time.time())

   transfer(self,other,self._tables_alias['constraint'][True],each)

   elapsed.append(time.time())

   print(f'{i} / {len(_constraint)} ' + '-' * 128, ' {:6.3f}s'.format(elapsed[-1]-elapsed[-2]))

#///
 
 def __init__(self,db_file=None,image_file={},auto_export_table=True):    # ===

  self.image_file = \
  {
    'original': '/project/at073-elderfall/collection/archive/DATA'
  , 'openpose': '/project/at073-elderfall/collection/data/OpenPose/keypoint-2019-01-31.tar'

  , **image_file
  }
  
  self.db_file     = db_file
  self._tables     = []
  self._constraint = None
  
  self.auto_export_table = auto_export_table
  
  if db_file is not None:
   
   self.db = SqliteExtDatabase(self.db_file)
   self.db.connect()

   self._tables = bind_table \
   (
     self.db
   , bind = self
   , KeypointBody25             = 'K'
   , BboxSSD                    = 'bS'
   , BboxMRCNN                  = 'bM'
   , DotMetaAnnotationFrameFall = 'A'
   , DotMetaNumberOfFrame       = 'nF'
   , DotMetaImageSize           = 'Z'
   , DotMetaArchiveVideoImage   = 'V'
   )

   self._tables_alias = { 'constraint': { True: [ 'K', 'bS', 'bM', 'A', 'nF', 'Z' ], False: [ 'V' ] } }

   if self.auto_export_table:
    
    self.export_table(1)

#///
    
 def constraint(self,_constraint=None,prune={},opt={},verbose=False):   # ===

  # { k: v for k, v in sorted([ [re.sub('^_','',_.lhs.name),_.rhs] for _ in constraint(K)],key=lambda _ : _[0])}

  # === ... setup
  
  if _constraint is None: _constraint = self._constraint
   
  if _constraint is None: return self

  elapsed = []
  
  def msg(_):
   if verbose:
    elapsed.append(time.time())
    print(' ... {:24s} '.format(_),end='')
  def done():
   if verbose:
    elapsed.append(time.time())
    print(' [DONE] ({:5.2f}s)'.format(elapsed[-1]-elapsed[-2]))
  
  self._constraint = _constraint
  
  opt_ = \
  {
    'tracking':
    {
      'tracking_IoU_threshold' : 0.1
    , 'tracking_step_threshold': 10
    , 'skip'                   : False
    , 'verbose'                : False
    , 'bbox'                   : 'SSD+MRCNN'
     
    , **getattr(opt,'tracking',{})
    }
  }
#///
  
  A, K, V, Z, bS, bM, nF = [ getattr(self,_) for _ in ['A','K','V','Z','bS','bM', 'nF'] ]
  
  self.reset()
  
  msg('shape')              # ===
  #
  # Height, width of the images
  #
  h, w = what1(select(Z,'height','width').where(*_constraint(Z)))
  
  self.h = h
  self.w = w
  self.shape = (h,w)

  done()
#///
  msg('annotation')         # ===
  #
  # Annotation: [ [start,end]... ] of frames of falls
  #
  self.annotation = what1(select(A,'frame').where(*_constraint(A)))
  
  done()
#///
  msg('keypoint')           # ===
  #
  # Keypoint coordinates. By default, we also add a skull cap point by
  # linear extrapolation from nose and neck if both are present.
  #
  kp = \
  [
   (f,pose_keypoints_2d(kp,skullcap=True))
    for f, kp in
     select(K,'frame','keypoint').where(*_constraint(K)).order_by(K._frame)
  ]

  #
  # Optionally select subset of the coordinates; for example, you might
  # want to look at only the covering ellipse for just the torso + head
  # ignoring the elbows, wrists, etc.
  #
  # kp = [ (f,pose_keypoints_2d(_,prune=['elbow','wrist'],confidence=False)) for f, _ in kp ]
  #
  if 'keypoint' in prune:
   
   kp = [ (f,pose_keypoints_2d(_,prune=prune['keypoint'],confidence=False)) for f, _ in kp ]

  self.kp = kp
  
  done()
#///

  # === Bounding boxes
  #
  # Bounding boxes
  #

  self.bbox = {}

  msg('bounding box (SSD)')       #
  self.bbox['SSD']    = bboxSSD  (what1(select(bS,'bbox').where(*_constraint(bS)))[0])
  done()

  msg('bounding box (MRCNN)')       #
  self.bbox['MRCNN']  = bboxMRCNN(what1(select(bM,'bbox').where(*_constraint(bM)))[0])
  done()
  
  msg('bounding box (combined)')       #
  self.bbox['SSD+MRCNN'] = [ [*a,*b] for a, b in zip(self.bbox['SSD'],self.bbox['MRCNN']) ]
  self.bbox['@']         = self.bbox['SSD+MRCNN']

  self.bbox['@'] = [ [] if len(_) == 0 else [i,ROI.group({ k: [v] for k, v in enumerate(_) })[2]['bbox_u']][1] for i,_ in enumerate(self.bbox['@']) ]
  done()

  self.bbox['MRCNN+SSD'] = self.bbox['@']
  self.bbox['SSD+MRCNN'] = self.bbox['@']
  
#///

  msg('keypoint <-> bbox')  # ===

# self.tracking = ssd_openpose_tracking_debug(self.bbox,self.kp,self.annotation,**opt_['tracking'])
  
  done()
#///
  msg('video db')           # ===
   
  self.vidb   = VideoImageDB(index=(Z,V),openpose=self.image_file['openpose'])
  
  self.stream = { 'openpose': self.vidb(_constraint,'openpose') }
  
  done()
#///

  return self

#///
  
 def reset(self):   # ===
  
  for _ in ['h','w','shape','kp','bbox','bboxSSD','bboxMRCNN','tracking','vidb','stream']:
   
   if hasattr(self,_):
    
    delattr(self,_)

  return self

#///

 def export_table(self,back=0): # ===
  
  encl_locals = inspect.currentframe().f_back
  
  for _ in range(back): encl_locals = encl_locals.f_back
   
  encl_locals = encl_locals.f_locals
  
  for _ in self._tables:
   
   encl_locals[_] = getattr(self,_)

  return self

#///
   
 def __del__(self): # ===
  
  encl_locals = inspect.currentframe().f_back.f_locals
  for _ in self._tables:
   if _ in encl_locals:
    del encl_locals[_]
    
  self.reset()

  ret = self.db.close()
  #print('DB connexion closed:',ret)

#///

 def __len__(self): # ===

  if not hasattr(self,'stream'):

   return 0

  return len(self.stream['openpose'])

#///

 def len(self):     # ===

  return len(self)

#///

 def matched(self): # ===

  print('_Matched_ not implemented in this version')

  if hasattr(self,'tracking'):

   max([ len(_[1]) for _ in self.tracking['verbose']])
   [ print(_[0]+' :: '.join(_[1])) for _ in self.tracking['verbose']]

#///

 def bboxStats(self,bins=20,density=False,verbose=False,figsize=(6,4)): # ===

  return check_bbox_support(self.stream['openpose'].shape,self.bbox['MRCNN'],self.bbox['SSD'],bins=bins,density=density,verbose=verbose,figsize=figsize)

#///

 def kpStats(self,frame=None,group_by='level',confidence=True,spacing=(2,0),truncate=3,verbose=False):  # ===

  if frame is None:

   frame = len(self.stream['openpose']) // 2

  n = len(self)
  if 0 < frame < 1:
   frame = int(n/2)
  i = frame
  if i >= 0:
   i = i % n
  else:
   i = - ( (-i) % n )

  frame_ = ( i + n ) % n

  if verbose:

   print(f'\nFrame: {frame_} ({i}) of {n}')

  return pose_keypoints_2d_parts_identified(self.kp[frame_],verbose=verbose,group_by=group_by,confidence=confidence,spacing=spacing,truncate=truncate);

#///

 def explore(self,frame=None,scale=1,linewidth=2,interactive=True,layout='TB'): # ===

  print('_Explore_ not implemented in this version')

  if frame is None:

   frame = len(self.stream['openpose']) // 2

# check_bbox_position(frame,self.stream['openpose'],[self.bbox['MRCNN'],self.bbox['SSD'],self.bbox['@']],scale=scale,linewidth=linewidth,interactive=interactive,keypoint=self.kp,tracking=self.tracking,layout=layout)

#///

 def plot(self,cut=0,s=np.s_[:],conf=True,anchor='hip',mode=1,aggregate=np.median,window=1,axis={'share':True,'show':False},xlim=None,ylim=(0,2),save=None,figsize=(30,16),figscale=1): # ===

  if isinstance(s,Number):

   s = np.s_[int(s):]

  elif isinstance(s,(list,tuple)):

   s = np.s_[s[0]:s[1]]

  elif isinstance(s,slice):

   pass

  else:

   s = np.s_[:]

  elapsed = time.time()
  plot_data(load_data(self.K,constraint=self._constraint,cut=cut),s=s,anchor=anchor,mode=mode,aggregate=aggregate,window=window,axis=axis,xlim=xlim,ylim=ylim,figsize=figsize,figscale=figscale)
  elapsed = time.time() - elapsed
  print(f'Elapsed: {elapsed:5.3f}s')

#///

 def play(self,scale=1):    # ===

  if hasattr(self,'stream'):

   h = int(self.h*scale)
   w = int(self.w*scale)

   return self.stream['openpose'].play(h,w)

#///

#///

